Proyek Bahasa Alami 
Program Studi   : Sistem Informasi 
Angkatan        : 2015
Judul           : Text Summarization Bahasa Indonesia using Maximum Marginal Relevance(MMR)
Disusun oleh    : 12S15038 - Riahta Mei Ulina Ketaren
                  12S15053 - Dwi Advenia Sinaga
                  12S15065 - Winner Immanuel Siringo-ringo

Environments/Tools needed :
Django Framework
Python 3 or updated
Jetbrains PyCharm
and several python's module

Happy sharing :)